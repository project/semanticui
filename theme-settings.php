<?php

/**
 * @file
 * Provides theme settings for Semantic based themes.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function semanticui_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state) {
  // Get the build info.
  $build_info = $form_state->getBuildInfo();
  $theme = !empty($build_info['args'][0]) ? $build_info['args'][0] : NULL;
  $form['semanticui'] = [
    '#prefix' => '<h2 class="ui dividing header">' . t('Semantic UI Settings') . '</h2>',
  ];
  $form['semanticui']['semanticui_popup_description'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable form fields popup descriptions'),
    '#default_value' => theme_get_setting('semanticui_popup_description', $theme),
  ];
  $form['semanticui']['semanticui_fixed_navbar'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable fixed navigation bar'),
    '#default_value' => theme_get_setting('semanticui_fixed_navbar', $theme),
  ];
  $form['semanticui']['semanticui_error_popups'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable form field error popups'),
    '#default_value' => theme_get_setting('semanticui_error_popups', $theme),
  ];
  $form['semanticui']['semanticui_floating_labels'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable text field floating labels'),
    '#default_value' => theme_get_setting('semanticui_floating_labels', $theme),
  ];
  $form['semanticui']['semanticui_enlarge_touchable_font'] = [
    '#type' => 'checkbox',
    '#title' => t('Enlarge font size on touch devices'),
    '#default_value' => theme_get_setting('semanticui_enlarge_touchable_font', $theme),
  ];
}
