Semantic UI - A base-theme for Drupal.

A theme based on the Semantic UI library (authored by Jack Lukic), which empowers designers and developers by creating a language for sharing UI. It is structured around natural language conventions to make development more intuitive.


Dependencies
------------
1. Libraries API - The theme uses Libraries API to to store the Semantic UI code as an external library. Please download the latest version of Semantic UI from Github and place all content of the /dist folder into a new libraries folder 'semanticui' within your drupal installation. You should end up with this folder structure:
   /sites/all/libraries/semanticui/components/...
   /sites/all/libraries/semanticui/themes/...
   /sites/all/libraries/semanticui/semantic.css
   /sites/all/libraries/semanticui/semantic.js
   /sites/all/libraries/semanticui/semantic.min.css
   /sites/all/libraries/semanticui/semantic.min.js

2. jQuery 1.7+ - Semantic UI requires a minimum jQuery version of 1.7 or higher. The preferred method is to install the jQuery Update (version 7.x-2.3 or higher) module. You must also configure the jQuery Update setting to this version after it is installed. configure the jQuery Update setting to this version after it is installed.
