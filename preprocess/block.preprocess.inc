<?php

/**
 * @file
 * Contains preprocess functions for block.
 */

use \Drupal\block\Entity\Block;
use \Drupal\block\BlockInterface;

/**
 * Implements hook_preprocess_hook().
 */
function semanticui_preprocess_block(&$variables) {
  $block_id = $variables['elements']['#id'] ?? NULL;
  /* @var BlockInterface $block */
  if ($block_id && ($block = Block::load($block_id))) {
    $variables['content']['#attributes']['block'] = $block_id;
    $variables['is_sidebar_region'] = in_array($block->getRegion(), ['sidebar_first', 'sidebar_second']);
  }
}
