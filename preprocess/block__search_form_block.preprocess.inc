<?php

/**
 * @file
 * Contains preprocess functions for block.
 */

/**
 * Implements hook_preprocess_hook().
 */
function semanticui_preprocess_block__search_form_block(&$variables) {
  require_once dirname(__FILE__) . '/block.preprocess.inc';
  semanticui_preprocess_block($variables);
}
