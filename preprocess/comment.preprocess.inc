<?php

/**
 * @file
 * Contains preprocess and related functions for comments.
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function semanticui_preprocess_comment(&$variables) {
  if ($variables['threaded'] && isset($variables['elements']['#subcomments'])) {
    $variables['subcomments'] = $variables['elements']['#subcomments'];
  }

  // Template comment.html.twig is used with default comments view (under the
  // node) and at the Reply page. With default view this template is rendered
  // inside field--comment.html.twig and has wrapper with "ui comments" classes,
  // but at the Reply page it doesn't.
  $variables['is_reply_page'] = \Drupal::routeMatch()->getRouteName() === 'comment.reply';
}
