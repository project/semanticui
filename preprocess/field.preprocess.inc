<?php

/**
 * @file
 * Contains preprocess functions for field.
 */

/**
 * Implements hook_preprocess_hook().
 */
function semanticui_preprocess_field(&$variables) {
  if ($variables['element']['#field_name'] === 'user_picture') {
    foreach ($variables['items'] as $key => $item) {
      $variables['items'][$key]['content']['#item_attributes']['class'][] = 'rounded';
    }
  }
}
