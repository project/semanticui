<?php
/**
 * @file
 * Functions to aid in presenting database results as a set of pages.
 */

use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

/**
 * Prepares variables for pager templates.
 *
 * Default template: pager.html.twig.
 *
 * Menu callbacks that display paged query results should use #type => pager
 * to retrieve a pager control so that users can view other results. Format a
 * list of nearby pages with additional query results.
 *
 * @param array $variables
 *   An associative array containing:
 *   - pager: A render element containing:
 *     - #tags: An array of labels for the controls in the pager.
 *     - #element: An optional integer to distinguish between multiple pagers on
 *       one page.
 *     - #parameters: An associative array of query string parameters to append
 *       to the pager links.
 *     - #route_parameters: An associative array of the route parameters.
 *     - #quantity: The number of pages in the list.
 */
function semanticui_preprocess_pager(&$variables) {
  $element = $variables['pager']['#element'];
  $parameters = $variables['pager']['#parameters'];
  $quantity = $variables['pager']['#quantity'];
  $route_name = $variables['pager']['#route_name'];
  $route_parameters = isset($variables['pager']['#route_parameters']) ? $variables['pager']['#route_parameters'] : [];
  global $pager_page_array, $pager_total;

  // Nothing to do if there is only one page.
  if ($pager_total[$element] <= 1) {
    return;
  }

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to.
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity).
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity).
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number.
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $items = [];
  // Create the "first" link if the loop doesn't generate the item.
  if ($i > 1) {
    $items['first'] = [];
    $options = [
      'query' => pager_query_add_page($parameters, $element, 0),
    ];
    $items['first']['href'] = Url::fromRoute($route_name, $route_parameters, $options)->toString();
    $items['first']['text'] = 1;
  }

  // Create the "previous" link if we are not on the first page. Template
  // renders "left arrow" for the "previous" item.
  if ($pager_page_array[$element] > 0) {
    $items['previous'] = [];
    $options = [
      'query' => pager_query_add_page($parameters, $element, $pager_page_array[$element] - 1),
    ];
    $items['previous']['href'] = Url::fromRoute($route_name, $route_parameters, $options)->toString();
  }

  // Reset ellipsis settings.
  $variables['ellipses']['previous'] = FALSE;
  $variables['ellipses']['next'] = FALSE;

  if ($i != $pager_max) {
    // Add an ellipsis if there are further previous pages.
    if ($i > 2) {
      $variables['ellipses']['previous'] = TRUE;
    }
    // Now generate the actual pager piece.
    for (; $i <= $pager_last && $i <= $pager_max; $i++) {
      $options = [
        'query' => pager_query_add_page($parameters, $element, $i - 1),
      ];
      $items['pages'][$i]['href'] = Url::fromRoute($route_name, $route_parameters, $options)->toString();
      if ($i == $pager_current) {
        $variables['current'] = $i;
      }
    }
    // Add an ellipsis if there are further next pages.
    if ($i < $pager_max) {
      $variables['ellipses']['next'] = TRUE;
    }
  }

  // Create the "next" link if we are not on the last page. Template renders
  // "right arrow" for the "next" item.
  if ($pager_page_array[$element] < ($pager_max - 1)) {
    $items['next'] = [];
    $options = [
      'query' => pager_query_add_page($parameters, $element, $pager_page_array[$element] + 1),
    ];
    $items['next']['href'] = Url::fromRoute($route_name, $route_parameters, $options)->toString();
  }

  // Create the "last" link if the loop didn't generate the item.
  if ($i <= $pager_max) {
    $items['last'] = [];
    $options = [
      'query' => pager_query_add_page($parameters, $element, $pager_max - 1),
    ];
    $items['last']['href'] = Url::fromRoute($route_name, $route_parameters, $options)->toString();
    $items['last']['text'] = $pager_max;
  }

  $variables['items'] = $items;
  $variables['heading_id'] = Html::getUniqueId('pagination-heading');

  // The rendered link needs to play well with any other query parameter used
  // on the page, like exposed filters, so for the cacheability all query
  // parameters matter.
  $variables['#cache']['contexts'][] = 'url.query_args';
}
