<?php

/**
 * @file
 * Implements hook_form_alter() to alter forms.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_form_alter().
 */
function semanticui_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id) {
    switch ($form_id) {
      case 'search_form':
        // Hide default submit button.
        $form['basic']['submit']['#attributes']['class'][] = 'visually-hidden';
        // Add placeholder to search input.
        $form['basic']['keys']['#attributes']['placeholder'] = t('Search ...');
        break;

      case 'search_block_form':
        // Hide default submit button.
        $form['actions']['submit']['#attributes']['class'][] = 'visually-hidden';
        // Add placeholder to search input.
        $form['keys']['#attributes']['placeholder'] = t('Search ...');
        break;

      case 'user_login_form':
      case 'user_register_form':
      case 'user_pass':
        $form['#attributes']['class'][] = 'segment';
        break;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function semanticui_form_comment_form_alter(&$form, FormStateInterface $form_state) {
  $form['#after_build'][] = 'semanticui_comments_after_build';

  $form['actions']['submit']['#theme_wrappers'] = ['input__labeled_button'];
  $form['actions']['submit']['#label'] = [
    '#prefix' => '<i class="icon edit"></i>',
    '#markup' => t('Add Comment'),
  ];
  $form['actions']['submit']['#attributes']['class'][] = 'primary';
  $form['actions']['submit']['#attributes']['class'][] = 'labeled';
  $form['actions']['submit']['#attributes']['class'][] = 'icon';
}

/**
 * After build function for the Comment form.
 *
 * Updates attributes that are not populated in the hook_form_alter().
 */
function semanticui_comments_after_build($form, &$form_state) {
  if (isset($form['comment_body'])) {
    // Move filter hint to Fomantic UI popup.
    $form['comment_body']['widget'][0]['format']['#attributes']['class'][] = 'ui';
    $form['comment_body']['widget'][0]['format']['#attributes']['class'][] = 'popup';
    $form['comment_body']['widget'][0]['value']['#title'] .= '&nbsp;<i class="help circle icon"></i>';
    $form['comment_body']['widget'][0]['#attached']['library'][] = 'semanticui/comment';
  }

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Update position of the Region dropdown to be rendered as the last element in
 * the wrapper. By default there is one more input with theme name which is
 * invisible but Fomantic UI adds extra margin to the select element.
 */
function semanticui_form_block_admin_display_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (empty($form['blocks'])) {
    return;
  }

  foreach ($form['blocks'] as $key => $block) {
    if (!isset($form['blocks'][$key]['region-theme']['region']) || !isset($form['blocks'][$key]['region-theme']['theme'])) {
      continue;
    }

    $region = $form['blocks'][$key]['region-theme']['region'];
    $form['blocks'][$key]['region-theme']['region'] = $form['blocks'][$key]['region-theme']['theme'];
    $form['blocks'][$key]['region-theme']['theme'] = $region;
  }
}
