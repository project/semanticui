<?php

/**
 * @file
 * Contains menu related theme functions.
 */

use Drupal\block\Entity\Block;

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function semanticui_theme_suggestions_menu_alter(array &$suggestions, array $variables) {
  if (isset($variables['attributes']['block'])) {
    $block = Block::load($variables['attributes']['block']);
    if ($block) {
      // Add theme suggestion to set vertical variation in sidebar menus.
      array_unshift($suggestions, 'menu__' . str_replace('-', '_', $block->getRegion()));
    }
  }
}
