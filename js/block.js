/**
 * @file
 * Overrides blockDrag method from Block module to use Semantic UI behaviour.
 *
 * As default onDrop method just sets the value to the "select" elements (region
 * name and block weight) it would be good to notify Dropdown module from the
 * Semantic UI framework that the element has been updated.
 */
(function ($) {
  Drupal.behaviors.blockDrag = {
    attach: function attach(context, settings) {
      if (typeof Drupal.tableDrag === 'undefined' || typeof Drupal.tableDrag.blocks === 'undefined') {
        return;
      }

      function checkEmptyRegions(table, rowObject) {
        table.find('tr.region-message').each(function () {
          var $this = $(this);

          if ($this.prev('tr').get(0) === rowObject.element) {
            if (rowObject.method !== 'keyboard' || rowObject.direction === 'down') {
              rowObject.swap('after', this);
            }
          }

          if ($this.next('tr').is(':not(.draggable)') || $this.next('tr').length === 0) {
            $this.removeClass('region-populated').addClass('region-empty');
          } else if ($this.is('.region-empty')) {
            $this.removeClass('region-empty').addClass('region-populated');
          }
        });
      }

      function updateLastPlaced(table, rowObject) {
        table.find('.color-success').removeClass('color-success');

        var $rowObject = $(rowObject);
        if (!$rowObject.is('.drag-previous')) {
          table.find('.drag-previous').removeClass('drag-previous');
          $rowObject.addClass('drag-previous');
        }
      }

      function updateBlockWeights(table, region) {
        var weight = -Math.round(table.find('.draggable').length / 2);

        table.find('.region-' + region + '-message').nextUntil('.region-title').find('select.block-weight').val(function () {
          return ++weight;
        }).parent().dropdown('set selected');
      }

      var table = $('#blocks');

      var tableDrag = Drupal.tableDrag.blocks;

      tableDrag.row.prototype.onSwap = function (swappedRow) {
        checkEmptyRegions(table, this);
        updateLastPlaced(table, this);
      };

      tableDrag.onDrop = function () {
        var dragObject = this;
        var $rowElement = $(dragObject.rowObject.element);

        var regionRow = $rowElement.prevAll('tr.region-message').get(0);
        var regionName = regionRow.className.replace(/([^ ]+[ ]+)*region-([^ ]+)-message([ ]+[^ ]+)*/, '$2');
        var regionField = $rowElement.find('select.block-region-select');

        if (regionField.find('option[value=' + regionName + ']').length === 0) {
          window.alert(Drupal.t('The block cannot be placed in this region.'));

          regionField.trigger('change');
        }

        if (!regionField.is('.block-region-' + regionName)) {
          var weightField = $rowElement.find('select.block-weight');
          var oldRegionName = weightField[0].className.replace(/([^ ]+[ ]+)*block-weight-([^ ]+)([ ]+[^ ]+)*/, '$2');
          regionField.removeClass('block-region-' + oldRegionName).addClass('block-region-' + regionName);
          weightField.removeClass('block-weight-' + oldRegionName).addClass('block-weight-' + regionName);
          regionField.val(regionName);
          regionField.parent().dropdown('set selected');
        }

        updateBlockWeights(table, regionName);
      };

      $(context).find('select.block-region-select').once('block-region-select').on('change', function (event) {
        var row = $(this).closest('tr');
        var select = $(this);

        tableDrag.rowObject = new tableDrag.row(row[0]);
        var regionMessage = table.find('.region-' + select[0].value + '-message');
        var regionItems = regionMessage.nextUntil('.region-message, .region-title');
        if (regionItems.length) {
          regionItems.last().after(row);
        } else {
          regionMessage.after(row);
        }

        updateBlockWeights(table, select[0].value);

        checkEmptyRegions(table, tableDrag.rowObject);

        updateLastPlaced(table, row);

        if (!tableDrag.changed) {
          $(Drupal.theme('tableDragChangedWarning')).insertBefore(tableDrag.table).hide().fadeIn('slow');
          tableDrag.changed = true;
        }

        select.trigger('blur');
      });
    }
  };
})(jQuery);
