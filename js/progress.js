/**
 * @file
 * Integrates Fomantic UI Progress behavior with Drupal Progress bar.
 */

(function ($, Drupal) {
  /**
   * Theme function for the progress bar.
   *
   * @see progress-bar.html.twig
   */
  Drupal.theme.progressBar = function (id) {
    return '<div id="' + id + '" class="ui blue active progress" aria-live="polite" data-percent="">' +
      '<div class="progress__bar bar"><div class="progress__percentage progress"></div></div>' +
      '<div class="progress__label label">&nbsp;</div>' +
      '<div class="progress__description">&nbsp;</div>' +
      '</div>';
  };

  $.extend(Drupal.ProgressBar.prototype, {
    /**
     * Set the percentage and status message for the progressbar.
     */
    setProgress: function setProgress(percentage, message, label) {
      if (percentage >= 0 && percentage <= 100) {
        $(this.element).attr('data-percent', percentage);
        $(this.element).find('div.progress__bar').css('width', percentage + '%');
        $(this.element).find('div.progress__percentage').html(percentage + '%');
      }
      $('div.progress__description', this.element).html(message);
      $('div.progress__label', this.element).html(label);
      if (this.updateCallback) {
        this.updateCallback(percentage, message, this);
      }
    },
  });
})(jQuery, Drupal);
