/**
 * @file
 * Initialize select elements.
 */
(function ($) {
  /**
   * Converts HTML select element to Semantic UI Dropdown.
   */
  Drupal.behaviors.semanticuiInitializeSelect = {
    attach: function (context, settings) {
      $('select.ui.dropdown', context).each(function () {
        // Keep initial element classes which libraries refer while updating
        // UI, e.g. dragging table rows (blockDrag).
        const classesToRemove = ['ui', 'dropdown'];
        let elementClasses = $(this).attr('class').split(/\s+/);
        elementClasses = $.grep(elementClasses, function (weightClass) {
          return $.inArray(weightClass, classesToRemove) === -1;
        }, false);

        $(this).dropdown({
          selectOnKeydown: false
        });

        // Assign required classes to the select elements and process related
        // wrapper. For example, at the Blocks page Drupal uses classes from the
        // select elements, so it is required to remove them from the wrapper
        // (they were automatically moved during initialization).
        $(this).addClass(elementClasses.join(' '));
        $(this).parent().removeClass(elementClasses.join(' '));
      });
    }
  };

  /**
   * Initializes dropdown button.
   */
  Drupal.behaviors.semanticuiInitializeDropdownButton = {
    attach: function (context, settings) {
      $('.button.ui.dropdown', context).dropdown();
    }
  };

  /**
   * Initializes dropdown menu.
   */
  Drupal.behaviors.semanticuiInitializeDropdownMenu = {
    attach: function (context, settings) {
      $('.ui.menu .item.dropdown', context).dropdown();
    }
  };

})(jQuery);
