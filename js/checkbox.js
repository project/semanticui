/**
 * @file
 * Initialize checkbox elements.
 */
(function ($) {
  /**
   * Updates checkbox elements to use full featured Semantic UI Checkbox.
   */
  Drupal.behaviors.semanticuiInitializeCheckbox = {
    attach: function (context, settings) {
      $('.ui.checkbox', context).each(function () {
        $(this).checkbox();
      });
    }
  };

})(jQuery);
