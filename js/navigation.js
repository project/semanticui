/**
 * @file
 * Initialize navigation bar when it's fixed.
 */
(function ($) {
  /**
   * Updates positioning of the navigation bar when it's fixed.
   */
  Drupal.behaviors.semanticuiNavigationBar = {
    attach: function (context, settings) {
      const initNavBar = function (offsets) {
        const $navBar = $('.navigation.menu');
        const barHeight = $navBar.outerHeight() || 0;
        const offsetTop = offsets.top || 0;
        $navBar.css('top', offsetTop);
        $('body').css('padding-top', offsetTop + barHeight + 'px');
      };

      initNavBar({});
      $(document).on('drupalViewportOffsetChange.semanticuiNavBar', function (e, offsets) {
        initNavBar(offsets);
      });
    }
  };

})(jQuery);
