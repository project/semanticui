/**
 * @file
 * Contains helper functions to work with Semantic UI theme.
 */

(function ($) {
  /**
   * Applies Dropdown module for each select element.
   *
   * Here is additional check for AJAX presence, it is used to properly
   * handle Drupal ajax classes with Semantic UI framework.
   *
   * Semantic UI creates div wrapper for select element, then applies
   * select's classes to this wrapper and finally removes all classes
   * from the select element. Such behaviour leads to situation when AJAX
   * callback will be called twice. It happens because Drupal
   * behaviours may be called multiple times and during first call in
   * $(element).dropdown() Semantic UI will remove element's classes.
   * After that Drupal will see (in the file misc/ajax.js in
   * Drupal.behaviors.AJAX) that there is an element with id that uses AJAX
   * but does not have AJAX class and will bind callback function to the
   * event that was specified in the FAPI #ajax element configuration
   * (e.g. 'change'). This will be second binding and callback will be
   * called twice that may break UI. See how Drupal binds the ajaxSubmit
   * function to the element event in the file misc/ajax.js in Drupal.ajax.
   */
  Drupal.behaviors.semanticuiDropdown = {
    attach: function(context) {
      $('select.ui.dropdown', context).each(function() {
        var hasAjax = $(this).hasClass('ajax-processed');

        // Semantic UI triggers .change event during the initialization phase.
        // With this behaviour AJAX select element will generate infinite
        // requests to the backend. Here is a short algorithm:
        // 1. user selects some value
        // 2. AJAX generates request to the backend
        // 3. backend responds with updated form elements
        // 4. Drupal calls registered behaviours
        // 5. semanticuiDropdown behaviour updates select element to use
        //    Semantic UI Dropdown module
        // 6. event .change is called during initialization the module
        // 7. things are being repeated from step 2
        // To remove infinite requests it would be good to unbind .change
        // events, then initialize Dropdown module and then assign
        // back .change events.
        if (hasAjax) {
          var selectEvents = $._data(this, 'events');
          var selectEventsCopy = $.extend(true, {}, selectEvents);
          $(this).unbind('change');
        }

        // Convert HTML select element to Semantic UI Dropdown.
        $(this).dropdown({
          selectOnKeydown: false
        });

        // Assign back change events.
        if (hasAjax) {
          var selectElement = this;
          $.each(selectEventsCopy, function() {
            $.each(this, function() {
              if (this.type != 'change') {
                return;
              }
              $(selectElement).bind(this.type, this.handler);
            });
          });

          // Apply required classes to select element.
          $(this).addClass('ajax-processed');
        }
      });
    }
  };

})(jQuery);
