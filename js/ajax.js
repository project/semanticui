(function ($) {

  /**
   * Prepare the Ajax request before it is sent.
   */
  Drupal.ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    // For forms without file inputs, the jQuery Form plugin serializes the form
    // values, and then calls jQuery's $.ajax() function, which invokes this
    // handler. In this circumstance, options.extraData is never used. For forms
    // with file inputs, the jQuery Form plugin uses the browser's normal form
    // submission mechanism, but captures the response in a hidden IFRAME. In this
    // circumstance, it calls this handler first, and then appends hidden fields
    // to the form to submit the values in options.extraData. There is no simple
    // way to know which submission mechanism will be used, so we add to extraData
    // regardless, and allow it to be ignored in the former case.
    if (this.form) {
      options.extraData = options.extraData || {};

      // Let the server know when the IFRAME submission mechanism is used. The
      // server can use this information to wrap the JSON response in a TEXTAREA,
      // as per http://jquery.malsup.com/form/#file-upload.
      options.extraData.ajax_iframe_upload = '1';

      // The triggering element is about to be disabled (see below), but if it
      // contains a value (e.g., a checkbox, textfield, select, etc.), ensure that
      // value is included in the submission. As per above, submissions that use
      // $.ajax() are already serialized prior to the element being disabled, so
      // this is only needed for IFRAME submissions.
      var v = $.fieldValue(this.element);
      if (v !== null) {
        options.extraData[this.element.name] = v;
      }
    }

    // Disable the element that received the change to prevent user interface
    // interaction while the Ajax request is in progress. ajax.ajaxing prevents
    // the element from triggering a new request, but does not prevent the user
    // from changing its value.
    $(this.element).addClass('progress-disabled').attr('disabled', true);

    // Do not show progress indicator for the element when it is not required.
    if ($(this.element).hasClass('no-throbber')) {
      return;
    }

    // Insert progressbar or throbber.
    if (this.progress.type == 'bar') {
      var progressBar = new Drupal.progressBar('ajax-progress-' + this.element.id, eval(this.progress.update_callback), this.progress.method, eval(this.progress.error_callback));
      if (this.progress.message) {
        progressBar.setProgress(-1, this.progress.message);
      }
      if (this.progress.url) {
        progressBar.startMonitoring(this.progress.url, this.progress.interval || 1500);
      }
      this.progress.element = $(progressBar.element).addClass('ajax-progress ajax-progress-bar');
      this.progress.object = progressBar;
      $(this.element).after(this.progress.element);
    }
    else if (this.progress.type == 'throbber' && $(this.element).parent().hasClass('dropdown')) {
      this.progress.element = $(Drupal.theme('dimmableThrobber'));

      var dimmableThrobber = new Drupal.dimmableThrobber(this.element.id);
      this.progress.object = dimmableThrobber;
      $(this.element).after(this.progress.element);

      // Remove Dimmer animation so that throbber will appear immediately.
      $(this.element).nextAll('.dimmer').css({
        'animation': 'none'
      });
      $(this.element).parent().dimmer({
        opacity: 1
      });
      $(this.element).parent().dimmer('show');
    }
    else if (this.progress.type == 'throbber' && $(this.element).hasClass('button') && $(this.element).parents('.dropdown').length) {
      var buttonThrobber = new Drupal.buttonThrobber(this.element.id);
      this.progress.object = buttonThrobber;
      $(this.element).parents('.dropdown').addClass('loading');
    }
    else if (this.progress.type == 'throbber' && $(this.element).hasClass('button')) {
      var buttonThrobber = new Drupal.buttonThrobber(this.element.id);
      this.progress.object = buttonThrobber;
      $(this.element).addClass('loading');
    }
    else if (this.progress.type == 'throbber' && $(this.element).hasClass('item') && $(this.element).closest('.menu').length) {
      this.progress.object = new Drupal.menuItemThrobber(this.element);
      this.progress.element = $(Drupal.theme('throbber'));
      $(this.element).prepend(this.progress.element);
    }
    else if (this.progress.type == 'throbber') {
      this.progress.element = $(Drupal.theme('throbber'));
      if (this.progress.message) {
        $('.throbber', this.progress.element).after('<div class="message">' + this.progress.message + '</div>');
      }
      $(this.element).after(this.progress.element);
    }
  };

  /**
   * Theme function for a throbber element.
   *
   * @return
   *   This function has to return HTML markup for the element.
   */
  Drupal.theme.prototype.throbber = function () {
    return '<div class="ui mini active inline loader"></div>';
  };

  /**
   * Theme function for a dimmable throbber element.
   *
   * @return
   *   This function has to return HTML markup for the element.
   */
  Drupal.theme.prototype.dimmableThrobber = function () {
    return '<div class="ui inverted dimmer"><div class="ui small loader"></div></div>';
  };

  /**
   * A dimmable throbber object. Initialized with the given id.
   */
  Drupal.dimmableThrobber = function (id) {
    this.id = id;
  };

  /**
   * Stop dimming via Ajax.
   */
  Drupal.dimmableThrobber.prototype.stopMonitoring = function () {
    $('#' + this.id).parent().removeClass('dimmable').removeClass('dimmed');
  };

  /**
   * A button throbber object. Initialized with the given id.
   */
  Drupal.buttonThrobber = function (id) {
    this.id = id;
  };

  /**
   * Stop loading animation.
   */
  Drupal.buttonThrobber.prototype.stopMonitoring = function () {
    $('#' + this.id).removeClass('loading');
  };

  /**
   * A menu item throbber object. Initialized with the given element.
   */
  Drupal.menuItemThrobber = function (element) {
    this.element = element;
    $(element).addClass('throbber-active');
    $(element).closest('.menu').addClass('throbber-active');
  };

  /**
   * Stop loading animation.
   */
  Drupal.menuItemThrobber.prototype.stopMonitoring = function () {
    $(this.element).removeClass('throbber-active');
    $(this.element).closest('.menu').removeClass('throbber-active');
  };

})(jQuery);
