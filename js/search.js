/**
 * @file
 * Initialize search input field.
 */
(function ($) {
  /**
   * Submit the form when user clicks on search icon.
   */
  Drupal.behaviors.semanticuiSearchInput = {
    attach: function(context) {
      $('.ui.search > .icon', context).click(function() {
        $(this).closest('form').submit();
      });
    }
  };

})(jQuery);
