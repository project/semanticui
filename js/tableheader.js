/**
 * @file
 * Sticky table headers.
 */

(function ($) {
  /**
   * Integrates Fomantic UI Table classes with Drupal sticky header.
   */
  Drupal.behaviors.semanticuiTableStickyHeader = {
    attach: function (context) {
      $.extend(Drupal.TableHeader.prototype, {
        createSticky: function createSticky() {
          var $stickyHeader = this.$originalHeader.clone(true);

          this.$stickyTable = $('<table class="sticky-header ui table"/>').css({
            visibility: 'hidden',
            position: 'fixed',
            top: '0px',
            'margin-top': '0px'
          }).append($stickyHeader).insertBefore(this.$originalTable);

          this.$stickyHeaderCells = $stickyHeader.find('> tr > th');

          this.recalculateSticky();
        }
      });
    }
  };

})(jQuery);
