/**
 * @file
 * Overrides default makeDraggable method with new handle markup.
 */
(function ($) {
  /**
   * Take an item and add event handlers to make it become draggable.
   */
  Drupal.tableDrag.prototype.makeDraggable = function (item) {
    var self = this;
    var $item = $(item);

    $item.find('td:first-of-type').find('a').addClass('menu-item__link');

    var handle = $('<i tabindex="0" class="tabledrag-handle move link icon"></i>').attr('title', Drupal.t('Drag to re-order'));

    var $indentationLast = $item.find('td:first-of-type').find('.js-indentation').eq(-1);
    if ($indentationLast.length) {
      $indentationLast.after(handle);

      self.indentCount = Math.max($item.find('.js-indentation').length, self.indentCount);
    } else {
      $item.find('td').eq(0).prepend(handle);
    }

    handle.on('mousedown touchstart pointerdown', function (event) {
      event.preventDefault();
      if (event.originalEvent.type === 'touchstart') {
        event = event.originalEvent.touches[0];
      }
      self.dragStart(event, self, item);
    });

    handle.on('click', function (e) {
      e.preventDefault();
    });

    handle.on('focus', function () {
      self.safeBlur = true;
    });

    handle.on('blur', function (event) {
      if (self.rowObject && self.safeBlur) {
        self.dropRow(event, self);
      }
    });

    handle.on('keydown', function (event) {
      if (event.keyCode !== 9 && !self.rowObject) {
        self.rowObject = new self.row(item, 'keyboard', self.indentEnabled, self.maxDepth, true);
      }

      var keyChange = false;
      var groupHeight = void 0;

      switch (event.keyCode) {
        case 37:
        case 63234:
          keyChange = true;
          self.rowObject.indent(-1 * self.rtl);
          break;

        case 38:
        case 63232:
        {
          var $previousRow = $(self.rowObject.element).prev('tr').eq(0);
          var previousRow = $previousRow.get(0);
          while (previousRow && $previousRow.is(':hidden')) {
            $previousRow = $(previousRow).prev('tr').eq(0);
            previousRow = $previousRow.get(0);
          }
          if (previousRow) {
            self.safeBlur = false;
            self.rowObject.direction = 'up';
            keyChange = true;

            if ($(item).is('.tabledrag-root')) {
              groupHeight = 0;
              while (previousRow && $previousRow.find('.js-indentation').length) {
                $previousRow = $(previousRow).prev('tr').eq(0);
                previousRow = $previousRow.get(0);
                groupHeight += $previousRow.is(':hidden') ? 0 : previousRow.offsetHeight;
              }
              if (previousRow) {
                self.rowObject.swap('before', previousRow);

                window.scrollBy(0, -groupHeight);
              }
            } else if (self.table.tBodies[0].rows[0] !== previousRow || $previousRow.is('.draggable')) {
              self.rowObject.swap('before', previousRow);
              self.rowObject.interval = null;
              self.rowObject.indent(0);
              window.scrollBy(0, -parseInt(item.offsetHeight, 10));
            }

            handle.trigger('focus');
          }
          break;
        }

        case 39:
        case 63235:
          keyChange = true;
          self.rowObject.indent(self.rtl);
          break;

        case 40:
        case 63233:
        {
          var $nextRow = $(self.rowObject.group).eq(-1).next('tr').eq(0);
          var nextRow = $nextRow.get(0);
          while (nextRow && $nextRow.is(':hidden')) {
            $nextRow = $(nextRow).next('tr').eq(0);
            nextRow = $nextRow.get(0);
          }
          if (nextRow) {
            self.safeBlur = false;
            self.rowObject.direction = 'down';
            keyChange = true;

            if ($(item).is('.tabledrag-root')) {
              groupHeight = 0;
              var nextGroup = new self.row(nextRow, 'keyboard', self.indentEnabled, self.maxDepth, false);
              if (nextGroup) {
                $(nextGroup.group).each(function () {
                  groupHeight += $(this).is(':hidden') ? 0 : this.offsetHeight;
                });
                var nextGroupRow = $(nextGroup.group).eq(-1).get(0);
                self.rowObject.swap('after', nextGroupRow);

                window.scrollBy(0, parseInt(groupHeight, 10));
              }
            } else {
              self.rowObject.swap('after', nextRow);
              self.rowObject.interval = null;
              self.rowObject.indent(0);
              window.scrollBy(0, parseInt(item.offsetHeight, 10));
            }

            handle.trigger('focus');
          }
          break;
        }
      }

      if (self.rowObject && self.rowObject.changed === true) {
        $(item).addClass('drag');
        if (self.oldRowElement) {
          $(self.oldRowElement).removeClass('drag-previous');
        }
        self.oldRowElement = item;
        if (self.striping === true) {
          self.restripeTable();
        }
        self.onDrag();
      }

      if (keyChange) {
        return false;
      }
    });

    handle.on('keypress', function (event) {

      switch (event.keyCode) {
        case 37:
        case 38:
        case 39:
        case 40:
          return false;
      }
    });
  };

  /**
   * Pointer event initiator, creates drag object and information.
   */
  Drupal.tableDrag.prototype.dragStart = function (event, self, item) {
    self.dragObject = {};
    self.dragObject.initOffset = self.getPointerOffset(item, event);
    self.dragObject.initPointerCoords = self.pointerCoords(event);
    if (self.indentEnabled) {
      self.dragObject.indentPointerPos = self.dragObject.initPointerCoords;
    }

    if (self.rowObject) {
      $(self.rowObject.element).find('i.tabledrag-handle').trigger('blur');
    }

    self.rowObject = new self.row(item, 'pointer', self.indentEnabled, self.maxDepth, true);

    self.table.topY = $(self.table).offset().top;
    self.table.bottomY = self.table.topY + self.table.offsetHeight;

    $(item).addClass('drag');

    $('body').addClass('drag');
    if (self.oldRowElement) {
      $(self.oldRowElement).removeClass('drag-previous');
    }
  };

  /**
   * {@inheritDoc}
   *
   * Triggers "change" event after setting new value to the Select element. The
   * event notifies Fomantic UI refresh Dropdown.
   */
  Drupal.tableDrag.prototype.updateField = function (changedRow, group) {
    var rowSettings = this.rowSettings(group, changedRow);
    var $changedRow = $(changedRow);
    var sourceRow = void 0;
    var $previousRow = void 0;
    var previousRow = void 0;
    var useSibling = void 0;

    if (rowSettings.relationship === 'self' || rowSettings.relationship === 'group') {
      sourceRow = changedRow;
    } else if (rowSettings.relationship === 'sibling') {
      $previousRow = $changedRow.prev('tr:first-of-type');
      previousRow = $previousRow.get(0);
      var $nextRow = $changedRow.next('tr:first-of-type');
      var nextRow = $nextRow.get(0);
      sourceRow = changedRow;
      if ($previousRow.is('.draggable') && $previousRow.find('.' + group).length) {
        if (this.indentEnabled) {
          if ($previousRow.find('.js-indentations').length === $changedRow.find('.js-indentations').length) {
            sourceRow = previousRow;
          }
        } else {
          sourceRow = previousRow;
        }
      } else if ($nextRow.is('.draggable') && $nextRow.find('.' + group).length) {
        if (this.indentEnabled) {
          if ($nextRow.find('.js-indentations').length === $changedRow.find('.js-indentations').length) {
            sourceRow = nextRow;
          }
        } else {
          sourceRow = nextRow;
        }
      }
    } else if (rowSettings.relationship === 'parent') {
      $previousRow = $changedRow.prev('tr');
      previousRow = $previousRow;
      while ($previousRow.length && $previousRow.find('.js-indentation').length >= this.rowObject.indents) {
        $previousRow = $previousRow.prev('tr');
        previousRow = $previousRow;
      }

      if ($previousRow.length) {
        sourceRow = $previousRow.get(0);
      } else {
        sourceRow = $(this.table).find('tr.draggable:first-of-type').get(0);
        if (sourceRow === this.rowObject.element) {
          sourceRow = $(this.rowObject.group[this.rowObject.group.length - 1]).next('tr.draggable').get(0);
        }
        useSibling = true;
      }
    }

    this.copyDragClasses(sourceRow, changedRow, group);
    rowSettings = this.rowSettings(group, changedRow);

    if (useSibling) {
      rowSettings.relationship = 'sibling';
      rowSettings.source = rowSettings.target;
    }

    var targetClass = '.' + rowSettings.target;
    var targetElement = $changedRow.find(targetClass).get(0);

    if (targetElement) {
      var sourceClass = '.' + rowSettings.source;
      var sourceElement = $(sourceClass, sourceRow).get(0);
      switch (rowSettings.action) {
        case 'depth':
          targetElement.value = $(sourceElement).closest('tr').find('.js-indentation').length;
          break;

        case 'match':
          targetElement.value = sourceElement.value;
          break;

        case 'order':
        {
          var siblings = this.rowObject.findSiblings(rowSettings);
          if ($(targetElement).is('select')) {
            var values = [];
            $(targetElement).find('option').each(function () {
              values.push(this.value);
            });
            var maxVal = values[values.length - 1];

            $(siblings).find(targetClass).each(function () {
              if (values.length > 0) {
                this.value = values.shift();
              } else {
                this.value = maxVal;
              }
              $(this).trigger('change');
            });
          } else {
            var weight = parseInt($(siblings[0]).find(targetClass).val(), 10) || 0;
            $(siblings).find(targetClass).each(function () {
              this.value = weight;
              weight++;
            });
          }
          break;
        }
      }
    }
  };

})(jQuery);
