/**
 * @file
 * Contains helper methods to update default Drupal vertical tabs and use
 * Fomantic UI Tabular menu.
 */

(function ($) {
  var handleFragmentLinkClickOrHashChange = function handleFragmentLinkClickOrHashChange(e, $target) {
    $target.parents('.vertical-tabs__pane').each(function (index, pane) {
      $(pane).data('verticalTab').focus();
    });
  };

  /**
   * Overwrites default Drupal verticalTabs behaviour.
   *
   * Integrates Fomantic UI Tabular menu instead of Drupal vertical tabs.
   */
  Drupal.behaviors.verticalTabs = {
    attach: function attach(context) {
      var width = drupalSettings.widthBreakpoint || 640;
      var mq = '(max-width: ' + width + 'px)';

      if (window.matchMedia(mq).matches) {
        return;
      }

      $('body').once('vertical-tabs-fragments').on('formFragmentLinkClickOrHashChange.verticalTabs', handleFragmentLinkClickOrHashChange);

      $(context).find('[data-vertical-tabs-panes]').once('vertical-tabs').each(function () {
        var $this = $(this).addClass('vertical-tabs__panes');
        var focusID = $this.find(':hidden.vertical-tabs__active-tab').val();
        var tabFocus = void 0;

        var $details = $this.find('> details');
        if ($details.length === 0) {
          return;
        }

        var tabList = $('<ul class="ui attached vertical tabular menu"></ul>');
        var tabListWrapper = $('<div class="four wide column"></div>').append(tabList);
        $this.wrap('<div class="ui basic vertical segment"></div>').wrap('<div class="ui grid"></div>').before(tabListWrapper);
        $this.addClass('twelve').addClass('wide').addClass('column');

        $details.each(function () {
          var $that = $(this);
          var verticalTab = new Drupal.verticalTab({
            title: $that.find('> summary').text(),
            details: $that
          });
          tabList.append(verticalTab.item);
          $that.removeClass('collapsed').attr('open', true).addClass('vertical-tabs__pane').data('verticalTab', verticalTab);
          if (this.id === focusID) {
            tabFocus = $that;
          }
        });

        $(tabList).find('> li').eq(0).addClass('first');
        $(tabList).find('> li').eq(-1).addClass('last');

        if (!tabFocus) {
          var $locationHash = $this.find(window.location.hash);
          if (window.location.hash && $locationHash.length) {
            tabFocus = $locationHash.closest('.vertical-tabs__pane');
          } else {
            tabFocus = $this.find('> .vertical-tabs__pane').eq(0);
          }
        }
        if (tabFocus.length) {
          tabFocus.data('verticalTab').focus();
        }
      });
    }
  };

  /**
   * Theme function to render a vertical tab.
   *
   * Uses Fomantic UI markup to draw Tabular menu.
   */
  Drupal.theme.verticalTab = function (settings) {
    var tab = {};
    tab.link = $('<a class="vertical-tabs__menu-item item" href="#"></a>')
    .append(tab.title = $('<strong></strong>').text(settings.title))
    .append(tab.summary = $('<span class="vertical-tabs__menu-item-summary"></span>'));

    tab.item = tab.link;
    return tab;
  };

  Drupal.verticalTab.prototype.focus = function () {
    this.details.siblings('.vertical-tabs__pane').each(function () {
      var tab = $(this).data('verticalTab');
      tab.details.hide();
      tab.item.removeClass('active');
    }).end().show().siblings(':hidden.vertical-tabs__active-tab').val(this.details.attr('id'));
    this.item.addClass('active');

    $('#active-vertical-tab').remove();
    this.link.append('<span id="active-vertical-tab" class="visually-hidden">' + Drupal.t('(active tab)') + '</span>');
  }
})(jQuery);
