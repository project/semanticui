/**
 * @file
 * Updates comment Body field to render text format inside Fomantic UI Popup.
 */
(function ($) {
  Drupal.behaviors.semanticuiCommentsPopup = {
    attach: function (context, settings) {
      // Use field wrapper instead of the form as on the regular node page
      // "context" represents complete form.
      const $icon = $('.js-form-item .help.icon', context);
      $icon.parent().popup({
        popup: '.ui.popup.filter-wrapper',
        delay: {
          hide: 200
        },
        hoverable: true
      });
    }
  };
})(jQuery);
