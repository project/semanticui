/**
 * @file
 * User permission page behaviors.
 */

(function ($) {
  /**
   * Overrides default behaviour and integrates Fomantic UI Checkbox.
   */
  Drupal.behaviors.permissions = {
    /**
     * Shows checked and disabled checkboxes for inherited permissions.
     */
    attach: function attach(context) {
      var self = this;
      $('table#permissions').once('permissions').each(function () {
        var $table = $(this);
        var $ancestor = void 0;
        var method = void 0;
        if ($table.prev().length) {
          $ancestor = $table.prev();
          method = 'after';
        } else {
          $ancestor = $table.parent();
          method = 'append';
        }
        $table.detach();

        var $dummy = $('<div class="ui checkbox"><input type="checkbox" class="dummy-checkbox js-dummy-checkbox" disabled="disabled" checked="checked" /></div>')
        .append('<label></label>')
        .attr('title', Drupal.t('This permission is inherited from the authenticated user role.'))
        .hide();

        $table.find('input[type="checkbox"]').not('.js-rid-anonymous, .js-rid-authenticated')
        .addClass('real-checkbox js-real-checkbox').each(function () {
          $dummy.clone().insertAfter($(this).parent());
        });

        $table.find('input[type=checkbox].js-rid-authenticated').on('change.permissions', self.toggle).each(self.toggle);

        $ancestor[method]($table);
      });
    },

    /**
     * Toggles all dummy checkboxes based on the checkboxes' state.
     *
     * If the "authenticated user" checkbox is checked, the checked and disabled
     * checkboxes are shown, the real checkboxes otherwise.
     */
    toggle: function toggle() {
      var authCheckbox = this;
      var $row = $(this).closest('tr');

      $row.find('.js-real-checkbox').each(function () {
        $(this).parent('.ui.checkbox').get(0).style.display = authCheckbox.checked ? 'none' : '';
      });
      $row.find('.js-dummy-checkbox').each(function () {
        $(this).parent('.ui.checkbox').get(0).style.display = authCheckbox.checked ? '' : 'none';
      });
    }
  };
})(jQuery);
