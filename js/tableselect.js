/**
 * @file
 * Updates default TableSelect behavior to support Fomantic UI checkboxes.
 */

(function ($, Drupal) {
  Drupal.tableSelect = function () {
    if ($(this).find('td input[type="checkbox"]').length === 0) {
      return;
    }

    var table = this;
    var checkboxes = void 0;
    var lastChecked = void 0;
    var $table = $(table);
    var strings = {
      selectAll: Drupal.t('Select all rows in this table'),
      selectNone: Drupal.t('Deselect all rows in this table')
    };
    var updateSelectAll = function updateSelectAll(state) {
      $table.prev('table.sticky-header').addBack().find('th.select-all input[type="checkbox"]').each(function () {
        var $checkbox = $(this);
        var stateChanged = $checkbox.prop('checked') !== state;

        $checkbox.attr('title', state ? strings.selectNone : strings.selectAll);

        if (stateChanged) {
          $checkbox.prop('checked', state).trigger('change');
        }
      });
    };

    $table.find('th.select-all').prepend($('<div class="ui checkbox"><input type="checkbox" class="form-checkbox" /><label></label></div>').attr('title', strings.selectAll)).on('click', function (event) {
      if ($(event.target).is('input[type="checkbox"]')) {
        checkboxes.each(function () {
          var $checkbox = $(this);
          var stateChanged = $checkbox.checkbox('is checked') !== event.target.checked;

          if (stateChanged) {
            if (event.target.checked) {
              $checkbox.checkbox('set checked');
            }
            else {
              $checkbox.checkbox('set unchecked');
            }
          }

          $checkbox.closest('tr').toggleClass('selected', $checkbox.checkbox('is checked'));
        });

        updateSelectAll(event.target.checked);
      }
    });

    checkboxes = $table.find('td .ui.checkbox').on('click', function (e) {
      $(this).closest('tr').toggleClass('selected', $(this).checkbox('is checked'));

      if (e.shiftKey && lastChecked && lastChecked !== e.target) {
        Drupal.tableSelectRange($(e.target).closest('tr')[0], $(lastChecked).closest('tr')[0], $(e.target).parent().checkbox('is checked'));
      }

      updateSelectAll(checkboxes.length === checkboxes.filter('.checked').length);

      lastChecked = e.target;
    });

    updateSelectAll(checkboxes.length === checkboxes.filter('.checked').length);
  };

  Drupal.tableSelectRange = function (from, to, state) {
    var mode = from.rowIndex > to.rowIndex ? 'previousSibling' : 'nextSibling';

    for (var i = from[mode]; i; i = i[mode]) {
      var $i = $(i);

      if (i.nodeType !== 1) {
        continue;
      }

      $i.toggleClass('selected', state);
      if (state) {
        $i.find('.ui.checkbox').checkbox('set checked');
      }
      else {
        $i.find('.ui.checkbox').checkbox('set unchecked');
      }

      if (to.nodeType) {
        if (i === to) {
          break;
        }
      } else if ($.filter(to, [i]).r.length) {
        break;
      }
    }
  };
})(jQuery, Drupal);
