/**
 * @file
 * Updates Drupal autocomplete to use Fomantic UI styles.
 */

(function ($) {
  /**
   * Initializes Search selection on autocomplete fields.
   */
  Drupal.behaviors.autocompleteDropdown = {
    attach: function (context, settings) {
      const $dropdowns = $('.search.selection.dropdown');
      $dropdowns.once('search-autocomplete').each(function () {
        const $dropdown = $(this);
        $dropdown.dropdown({
          showOnFocus: false,
          selectOnKeydown: false,
          allowAdditions: true,
          forceSelection: false,
          onChange: function (value, text, $choice) {
            $('> [data-autocomplete-path]', $dropdown).val(value).trigger('change');
          },
          apiSettings: {
            url: $dropdown.find('> [data-autocomplete-path]').attr('data-autocomplete-path') + '?q={query}',
            onResponse: function (drupalResponse) {
              if (!drupalResponse) {
                return;
              }

              let response = {
                results: []
              };
              for (let key in drupalResponse) {
                response.results.push({
                  'name': drupalResponse[key].label,
                  'value': drupalResponse[key].value
                });
              }

              return response;
            }
          }
        });

        // Clear value if the field is empty and backspace or del key was hit.
        $dropdown.find('.search').on('keyup', function (event) {
          const keys = {
            backspace: 8,
            del: 46
          };
          if ($(this).val() === '' && (event.which === keys.backspace || event.which === keys.del)) {
            $dropdown.dropdown('clear');
          }
        });
      });
    }
  };

  Drupal.behaviors.autocompleteDisableSearch = {
    attach: function (context, settings) {
      const $autocompleteField = $('.field.disabled.autocomplete', context);
      if ($autocompleteField.length) {
        $('input.search', $autocompleteField).addClass('disabled').attr('disabled', 'disabled');
      }
    }
  };
})(jQuery);
