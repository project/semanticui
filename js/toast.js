/**
 * @file
 * Initialize Toast elements.
 */
(function ($) {
  /**
   * Updates messages to be shown as a Toast.
   */
  Drupal.behaviors.semanticuiToast = {
    attach: function (context, settings) {
      // Apply Toast only once to fix case when AJAX form is submitted and
      // re-rendered again. In this case previous errors still exist on the page
      // and JavaScript behavior applies the Toast one more time. Also, update
      // form messages to be closed by clicking .close icon.
      $('.status-message', context).once('toast-message').each(function () {
        $(this).toast({
          position: 'top center',
          displayTime: $(this).closest('.ui.form').length ? 0 : 'auto'
        });
      });
    }
  };

})(jQuery);
