/**
 * @file
 * Update form field description to be shown in the popup message.
 */
(function ($) {
  /**
   * Initialize popup with field description.
   */
  Drupal.behaviors.semanticuiPopupDescription = {
    attach: function (context, settings) {
      $('.ui.form .popup.description', context).each(function () {
        const $popup = $(this);
        // Add popup to complete field wrapper.
        $(this).parent().popup({
          popup: $popup,
          on: 'click',
          hoverable: true,
          delay: {
            show: 300,
            hide: 800
          }
        });
      });
    }
  };

})(jQuery);
